package cz.fel.storedav.swa.reservations.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.net.URI;
import java.time.OffsetDateTime;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer>{
    List<Reservation> findByEndTimeGreaterThanEqualAndStartTimeLessThanEqualAndPlaneIsAndValidIsTrue(OffsetDateTime start, OffsetDateTime end, URI plane);
    List<Reservation> findByEndTimeGreaterThanEqualAndStartTimeLessThanEqual(OffsetDateTime start, OffsetDateTime end);
    List<Reservation> findByEndTimeGreaterThanAndValidIsTrue(OffsetDateTime now);


    long countByEndTimeGreaterThanEqualAndStartTimeLessThanEqualAndPlaneIsAndValidIsTrue(OffsetDateTime start, OffsetDateTime end, URI plane);
}
