
package cz.fel.storedav.swa.reservations.model.mappers;

import cz.fel.storedav.swa.api.model.ReservationShortDto;
import cz.fel.storedav.swa.api.model.ReservationDto;
import cz.fel.storedav.swa.reservations.model.Reservation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.web.util.UriTemplate;

@Mapper
public interface ReservationMapper {
    public static ReservationMapper INSTANCE = Mappers.getMapper(ReservationMapper.class);

    @Mapping(target="id", source = "id")
    @Mapping(target="plane", source = "reservationDto.plane")
    @Mapping(target="startTime", source = "reservationDto.startTime")
    @Mapping(target="endTime", source = "reservationDto.endTime")
    @Mapping(target="pilot", source = "reservationDto.pilot")
    @Mapping(target="instructor", source = "reservationDto.instructor")
    @Mapping(target="valid", source = "reservationDto.valid")
    @Mapping(target="type", source = "reservationDto.type")
    Reservation mapTo(ReservationDto reservationDto, Integer id);

    @Mapping(target="plane", source = "reservation.plane")
    @Mapping(target="startTime", source = "reservation.startTime")
    @Mapping(target="endTime", source = "reservation.endTime")
    @Mapping(target="pilot", source = "reservation.pilot")
    @Mapping(target="instructor", source = "reservation.instructor")
    @Mapping(target="valid", source = "reservation.valid")
    @Mapping(target="type", source = "reservation.type")
    ReservationDto mapTo(Reservation reservation);


    @Mapping(target="plane", source = "reservation.plane")
    @Mapping(target="startTime", source = "reservation.startTime")
    @Mapping(target="endTime", source = "reservation.endTime")
    @Mapping(target="reservation", expression = "java(base_uri.expand(reservation.getId()))")
    ReservationShortDto mapToShort(Reservation reservation, UriTemplate base_uri);


}
