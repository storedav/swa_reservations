package cz.fel.storedav.swa.reservations.api;

import cz.fel.storedav.swa.members_client.api.MemberApi;
import cz.fel.storedav.swa.members_client.api.MemberApiClient;
import cz.fel.storedav.swa.members_client.api.MembersApiClient;
import cz.fel.storedav.swa.members_client.model.MemberDto;
import cz.fel.storedav.swa.members_client.model.MemberShortDto;
import cz.fel.storedav.swa.members_client.model.MemberStatusDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.List;

@FeignClient(
        contextId = "Member",
        name = "${membersreg.name:members-register}",
        url = "${membersreg.url}",
        fallback = MemberServiceApiClient.MembersServiceApiClientFallback.class,
        configuration = MembersApiConfiguration.class
)
public interface MemberServiceApiClient extends MemberApi {
    Logger logger = LoggerFactory.getLogger(MemberServiceApiClient.class);

    @Component
    class MembersServiceApiClientFallback implements MemberServiceApiClient {
        @Override
        public ResponseEntity<MemberDto> memberMemberIdGet(Integer memberId) {
            logger.warn("memberMemberIdGet fallback");
            return ResponseEntity.notFound().build();
        }

        @Override
        public ResponseEntity<MemberStatusDto> memberMemberIdStateGet(Integer memberId) {
            logger.warn("memberMemberIdStateGet fallback");
            return ResponseEntity.notFound().build();
        }

        @Override
        public ResponseEntity<Void> memberMemberIdStatePut(Integer memberId, MemberStatusDto memberStatusDto) {
            logger.warn("memberMemberIdStatePut fallback");
            throw new UnsupportedOperationException("memberMemberIdStatePut should not be called here...");
        }
    }
}
