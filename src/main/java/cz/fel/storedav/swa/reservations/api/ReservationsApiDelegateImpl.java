package cz.fel.storedav.swa.reservations.api;

import cz.fel.storedav.swa.api.ReservationsApiDelegate;
import cz.fel.storedav.swa.api.model.ReservationDto;
import cz.fel.storedav.swa.api.model.ReservationShortDto;
import cz.fel.storedav.swa.members_client.model.MemberStatusDto;
import cz.fel.storedav.swa.planes_client.model.PlaneDto;
import cz.fel.storedav.swa.reservations.model.Reservation;
import cz.fel.storedav.swa.reservations.model.ReservationRepository;
import cz.fel.storedav.swa.reservations.model.mappers.ReservationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriTemplate;

import java.net.URI;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class ReservationsApiDelegateImpl implements ReservationsApiDelegate {

    final Logger logger = LoggerFactory.getLogger(ReservationsApiDelegateImpl.class);
    private final ReservationRepository resRepo;

    @Autowired
    private PlaneServiceApiClient planeServiceApiClient;

    @Autowired
    private MemberServiceApiClient memberServiceApiClient;

    public ReservationsApiDelegateImpl(ReservationRepository resRepo) {
        this.resRepo = resRepo;
    }


    @Override
    public ResponseEntity<List<ReservationShortDto>> reservationsGet(OffsetDateTime startDate, OffsetDateTime endDate, URI crewmember, URI plane) {
        logger.trace("GET reservations list");

        logger.trace("startDate: " + (startDate == null ? "null" : startDate.format(DateTimeFormatter.ISO_DATE)) + ", endDate: " + (endDate == null ? "null" : endDate.format(DateTimeFormatter.ISO_DATE)) + ", member: " + (crewmember == null ? "null" : crewmember.toString()) + ", plane: " + (plane == null ? "null" : plane.toString()));

        List<Reservation> foundReservations = resRepo.findByEndTimeGreaterThanEqualAndStartTimeLessThanEqual(
                startDate == null ? OffsetDateTime.of(1970,1,1, 0, 0, 0, 0, ZoneOffset.ofHours(0)) : startDate,
                endDate == null ? OffsetDateTime.of(2100,12,31, 23, 59, 59, 999999999, ZoneOffset.ofHours(0)) : endDate);

        if(plane != null){
            foundReservations = foundReservations.stream().filter(
                    reservation -> reservation.getPlane().equals(plane)
            ).toList();
        }

        if (foundReservations.isEmpty()) {
            throw new ResponseStatusException(
                    HttpStatus.NO_CONTENT, "No reservations with specified parameters was found."
            );
        }

        final UriTemplate reservationUriTeamplate = new UriTemplate(ServletUriComponentsBuilder.fromCurrentContextPath().path("/reservation/").toUriString() + "{reservationId}");

        final List<ReservationShortDto> members = foundReservations.stream().filter(
                reservation -> crewmember==null || crewmember.toString().isEmpty() || reservation.getPilot().equals(crewmember) || reservation.getInstructor().equals(crewmember)
        ).map(
                reservation -> ReservationMapper.INSTANCE.mapToShort(reservation, reservationUriTeamplate)
        ).toList();

        return ResponseEntity.ok(members);
    }

    @Override
    public ResponseEntity<URI> reservationsPost(ReservationDto reservationDto) {

        validateReservation(reservationDto);

        Reservation reservation = ReservationMapper.INSTANCE.mapTo(reservationDto, null);

        Reservation savedRes = resRepo.save(reservation);

        final URI planeUri =
                ServletUriComponentsBuilder.fromCurrentContextPath().path("/reservation/").path(savedRes.getId().toString()).build().toUri();
        return ResponseEntity.created(planeUri).body(planeUri);
    }

     boolean validateReservation(ReservationDto reservationDto){
         PlaneDto plane = null;
         MemberStatusDto pilot = null;
         MemberStatusDto instructor = null;


         if(reservationDto.getPlane().toString().isEmpty()){
            logger.warn("Plane not specified");
             throw new ResponseStatusException(
                     HttpStatus.NOT_ACCEPTABLE, "Plane not specified"
             );
         }
         ResponseEntity<PlaneDto> planeResponse = planeServiceApiClient.planePlaneIdGet(getPlaneId(reservationDto.getPlane()));

         if(planeResponse.getStatusCode() != HttpStatus.OK){
            logger.warn("Plane not found");
             throw new ResponseStatusException(
                     HttpStatus.NOT_ACCEPTABLE, "Plane not found"
             );
         }
         plane = planeResponse.getBody();

         if(!plane.getActive()){
            logger.warn("Plane not active");
             throw new ResponseStatusException(
                     HttpStatus.NOT_ACCEPTABLE, "Plane not active"
             );
         }

         if(reservationDto.getType().equals("personal") || reservationDto.getType().equals("school")){
             if(reservationDto.getPilot().toString().isEmpty()){
                logger.warn("Pilot not specified");
                 throw new ResponseStatusException(
                         HttpStatus.NOT_ACCEPTABLE, "Pilot not specified"
                 );
             }
             ResponseEntity<MemberStatusDto> pilotResponse = memberServiceApiClient.memberMemberIdStateGet(getMemberId(reservationDto.getPilot()));
             if(pilotResponse.getStatusCode() != HttpStatus.OK){
                logger.warn("Pilot not found");
                 throw new ResponseStatusException(
                         HttpStatus.NOT_ACCEPTABLE, "Pilot not found"
                 );
             }
             pilot = pilotResponse.getBody();
         }

         if(reservationDto.getType().equals("demo") || reservationDto.getType().equals("school")){
             if(reservationDto.getPilot().toString().isEmpty()){
                logger.warn("Instructor not specified");
                 throw new ResponseStatusException(
                         HttpStatus.NOT_ACCEPTABLE, "Instructor not specified"
                 );
             }

             ResponseEntity<MemberStatusDto> instrResponse = memberServiceApiClient.memberMemberIdStateGet(getMemberId(reservationDto.getPilot()));

             if(instrResponse.getStatusCode() != HttpStatus.OK){
                logger.warn("Instructor not found");
                 throw new ResponseStatusException(
                         HttpStatus.NOT_ACCEPTABLE, "Instructor not found"
                 );
             }
             instructor = instrResponse.getBody();
         }

         if(reservationDto.getType().equals("personal") && !pilot.getStatus().equals("active")){
            logger.warn("Pilot can't fly personal flights");
             throw new ResponseStatusException(
                     HttpStatus.NOT_ACCEPTABLE, "Pilot can't fly personal flights"
             );
         }

         if(reservationDto.getType().equals("demo") && !instructor.getStatus().equals("active")){
            logger.warn("Instructor can't fly demo flights");
             throw new ResponseStatusException(
                     HttpStatus.NOT_ACCEPTABLE, "Instructor can't fly demo flights"
             );
         }

         if(reservationDto.getType().equals("school") && (!instructor.getStatus().equals("active") || !pilot.getStatus().equals("student"))){
            logger.warn("Invalid crew");
             throw new ResponseStatusException(
                     HttpStatus.NOT_ACCEPTABLE, "Invalid crew"
             );
         }

         if(pilot!= null && !pilot.getStatus().equals("student") && !canFlyCategory(pilot, plane)){
            logger.warn("Pilot does not have license for the plane");
             throw new ResponseStatusException(
                     HttpStatus.NOT_ACCEPTABLE, "Pilot does not have license for the plane"
             );
         }

         if(instructor!= null && !canFlyCategory(instructor, plane)){
            logger.warn("Instructor does not have license for the plane");
             throw new ResponseStatusException(
                     HttpStatus.NOT_ACCEPTABLE, "Instructor does not have license for the plane"
             );
         }


         if(resRepo.countByEndTimeGreaterThanEqualAndStartTimeLessThanEqualAndPlaneIsAndValidIsTrue(
                 reservationDto.getStartTime() == null ? OffsetDateTime.of(1970,1,1, 0, 0, 0, 0, ZoneOffset.ofHours(0)) : reservationDto.getStartTime(),
                 reservationDto.getEndTime() == null ? OffsetDateTime.of(2100,12,31, 23, 59, 59, 999999999, ZoneOffset.ofHours(0)) : reservationDto.getEndTime(),
                 reservationDto.getPlane()) > 0){
             throw new ResponseStatusException(
                     HttpStatus.CONFLICT, "Reservation is in conflict with some other existing reservation"
             );
         }

         return true;
     }



    Integer getPlaneId(URI planeUri){
        try{
            var parts = planeUri.getPath().split("/");
            return Integer.parseInt(parts[parts.length - 1]);
        }catch(Exception ex){
            throw new ResponseStatusException(
                    HttpStatus.NOT_ACCEPTABLE, "Malformed plane uri"
            );
        }
    }

    Integer getMemberId(URI memberUri){
        try {
            var parts = memberUri.getPath().split("/");
            return Integer.parseInt(parts[parts.length - 1]);
        }catch(Exception ex){
            throw new ResponseStatusException(
                    HttpStatus.NOT_ACCEPTABLE, "Malformed member uri"
            );
        }
    }

    boolean canFlyCategory(MemberStatusDto member, PlaneDto plane){
        List<String> licenses = member.getLicenses();
        String type = plane.getCategory();

        for(String license : licenses){
            if (license.contains(type)){
                return true;
            }
        }

        return false;
    }


}
