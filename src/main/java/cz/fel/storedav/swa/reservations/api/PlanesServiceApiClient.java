package cz.fel.storedav.swa.reservations.api;

import cz.fel.storedav.swa.planes_client.api.PlaneApiClient;
import cz.fel.storedav.swa.planes_client.api.PlanesApi;
import cz.fel.storedav.swa.planes_client.api.PlanesApiClient;
import cz.fel.storedav.swa.planes_client.model.PlaneDto;
import cz.fel.storedav.swa.planes_client.model.PlanePriceDto;
import cz.fel.storedav.swa.planes_client.model.PlaneShortDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

@FeignClient(
        contextId = "Planes",
        name = "${planesreg.name:planes-register}",
        url = "${planesreg.url}",
        fallback = PlanesServiceApiClient.PlanesServiceApiClientFallback.class,
        configuration = PlanesApiConfiguration.class
)
public interface PlanesServiceApiClient extends PlanesApi {

    Logger logger = LoggerFactory.getLogger(PlanesServiceApiClient.class);

    @Component
    class PlanesServiceApiClientFallback implements PlanesServiceApiClient {

        @Override
        public ResponseEntity<List<PlaneShortDto>> planesGet(String type, String registration, String category) {
            logger.warn("fallback planesGet");
            return ResponseEntity.notFound().build();
        }

        @Override
        public ResponseEntity<URI> planesPost(PlaneDto planeDto) {
            logger.warn("fallback planesPost");
            throw new UnsupportedOperationException("planesPost should not be called here...");
        }
    }
}
