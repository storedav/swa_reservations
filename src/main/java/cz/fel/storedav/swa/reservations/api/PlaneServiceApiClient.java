package cz.fel.storedav.swa.reservations.api;

import cz.fel.storedav.swa.planes_client.api.PlaneApi;
import cz.fel.storedav.swa.planes_client.api.PlaneApiClient;
import cz.fel.storedav.swa.planes_client.api.PlanesApiClient;
import cz.fel.storedav.swa.planes_client.model.PlaneDto;
import cz.fel.storedav.swa.planes_client.model.PlanePriceDto;
import cz.fel.storedav.swa.planes_client.model.PlaneShortDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

@FeignClient(
        contextId = "Plane",
        name = "${planesreg.name:planes-register}",
        url = "${planesreg.url}",
        fallback = PlaneServiceApiClient.PlanesServiceApiClientFallback.class,
        configuration = PlanesApiConfiguration.class
)
public interface PlaneServiceApiClient extends PlaneApi {

    Logger logger = LoggerFactory.getLogger(PlaneServiceApiClient.class);

    @Component
    class PlanesServiceApiClientFallback implements PlaneServiceApiClient {
        @Override
        public ResponseEntity<PlaneDto> planePlaneIdGet(Integer planeId) {
            logger.warn("fallback planePlaneIdGet");
            return ResponseEntity.notFound().build();
        }

        @Override
        public ResponseEntity<PlanePriceDto> planePlaneIdPricePriceIdGet(Integer planeId, Integer priceId) {
            logger.warn("fallback planePlaneIdPricePriceIdGet");
            return ResponseEntity.notFound().build();
        }

        @Override
        public ResponseEntity<List<PlanePriceDto>> planePlaneIdPricesGet(Integer planeId, LocalDate dateStart, LocalDate dateEnd) {
            logger.warn("fallback planePlaneIdPricesGet");
            return ResponseEntity.notFound().build();
        }

        @Override
        public ResponseEntity<URI> planePlaneIdPricesPost(Integer planeId, PlanePriceDto planePriceDto) {
            logger.warn("fallback planePlaneIdPricesPost");
            throw new UnsupportedOperationException("planePlaneIdPricesPost should not be called here...");
        }

        @Override
        public ResponseEntity<Void> planePlaneIdPut(Integer planeId, PlaneDto planeDto) {
            logger.warn("fallback planePlaneIdPut");
            throw new UnsupportedOperationException("planePlaneIdPut should not be called here...");
        }
    }
}
