package cz.fel.storedav.swa.reservations;

import com.fasterxml.jackson.databind.Module;
import cz.fel.storedav.swa.planes_client.model.PlaneShortDto;
import cz.fel.storedav.swa.reservations.api.PlanesServiceApiClient;
import org.openapitools.jackson.nullable.JsonNullableModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FullyQualifiedAnnotationBeanNameGenerator;
import org.springframework.cloud.openfeign.EnableFeignClients;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;


@SpringBootApplication(
        nameGenerator = FullyQualifiedAnnotationBeanNameGenerator.class
)
@ComponentScan(
        basePackages = {"cz.fel.storedav.swa", "cz.fel.storedav.swa.api" , "org.openapitools.configuration"},
        nameGenerator = FullyQualifiedAnnotationBeanNameGenerator.class
)
@EnableDiscoveryClient
@EnableFeignClients
public class Application {

    Logger logger = LoggerFactory.getLogger(Application.class);

//    @Autowired
//    private PlanesServiceApiClient planesServiceApiClient;

    @Autowired
    private DiscoveryClient discoveryClient;
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

//    @PostConstruct
//    public void start() {
//        logger.info("Starting ...");
//        // get the information about the service programmatically
//        try {
//            List<ServiceInstance> instances = this.discoveryClient.getInstances("planes-register");
//            List<String> uris = instances.stream().map(i -> i.getUri().toString()).collect(Collectors.toList());
//            logger.info("planes-register running at {}", String.join(", ", uris));
//
//            List<String> services = this.discoveryClient.getServices();
//            logger.info("available services: {}", String.join(", ", services));
//
////            logger.error("Test client: "+ client.hello());
//
//            logger.info("Get planes: \n\t{}", this.planesServiceApiClient.planesGet("%", "%", "%"));
//            throw new Exception("STOPPER");
////            System.exit(0);
//        }
//        catch (Throwable t) {
//            logger.warn("An error has occurred in planes-service discovery", t);
//        }
//    }

    @Bean(name = "cz.fel.storedav.swa.Application.jsonNullableModule")
    public Module jsonNullableModule() {
        return new JsonNullableModule();
    }
}
