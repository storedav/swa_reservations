package cz.fel.storedav.swa.reservations.api;

import cz.fel.storedav.swa.api.ReservationApiDelegate;
import cz.fel.storedav.swa.api.ReservationsApiDelegate;
import cz.fel.storedav.swa.api.model.ReservationDto;
import cz.fel.storedav.swa.api.model.ReservationShortDto;
import cz.fel.storedav.swa.members_client.model.MemberStatusDto;
import cz.fel.storedav.swa.planes_client.model.PlaneDto;
import cz.fel.storedav.swa.reservations.model.Reservation;
import cz.fel.storedav.swa.reservations.model.ReservationRepository;
import cz.fel.storedav.swa.reservations.model.mappers.ReservationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriTemplate;

import java.net.URI;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationApiDelegateImpl implements ReservationApiDelegate {

    final Logger logger = LoggerFactory.getLogger(ReservationApiDelegateImpl.class);
    private final ReservationRepository resRepo;

    public ReservationApiDelegateImpl(ReservationRepository resRepo) {
        this.resRepo = resRepo;
    }


    @Override
    public ResponseEntity<ReservationDto> reservationReservationIdGet(Integer reservationId) {
        logger.trace("GET reservation");

        Optional<Reservation> reservation = resRepo.findById(reservationId);

        if (reservation.isEmpty()) {
            throw new ResponseStatusException(
                    HttpStatus.NO_CONTENT, "No reservation with specified id found."
            );
        }

        return ResponseEntity.ok(ReservationMapper.INSTANCE.mapTo(reservation.get()));
    }
}
