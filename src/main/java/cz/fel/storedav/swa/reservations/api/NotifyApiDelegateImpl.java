package cz.fel.storedav.swa.reservations.api;

import cz.fel.storedav.swa.api.NotifyApiDelegate;
import cz.fel.storedav.swa.api.ReservationsApiDelegate;
import cz.fel.storedav.swa.api.model.MemberStatusDto;
import cz.fel.storedav.swa.api.model.PlaneCategoryDto;
import cz.fel.storedav.swa.api.model.URIMemberStatusDto;
import cz.fel.storedav.swa.planes_client.model.PlaneDto;
import cz.fel.storedav.swa.reservations.model.Reservation;
import cz.fel.storedav.swa.reservations.model.ReservationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Service
public class NotifyApiDelegateImpl implements NotifyApiDelegate {

    final Logger logger = LoggerFactory.getLogger(ReservationsApiDelegateImpl.class);
    private final ReservationRepository resRepo;

    @Autowired
    private PlaneServiceApiClient planeServiceApiClient;

    @Autowired
    private MemberServiceApiClient memberServiceApiClient;

    public NotifyApiDelegateImpl(ReservationRepository resRepo) {
        this.resRepo = resRepo;
    }

    @Override
    public ResponseEntity<Void> notifyChangeMemberPost(URIMemberStatusDto memberStatusDto) {
        logger.info("NOTIFY notifyChangeMemberPost");

        List<Reservation> reservations = resRepo.findByEndTimeGreaterThanAndValidIsTrue(OffsetDateTime.now(ZoneOffset.ofHours(0)));
        
        logger.trace("NOTIFY reservationsToUpdate before filter:" + reservations.size());

        reservations = reservations.stream().filter(
                reservation -> (reservation.getPilot()!= null && reservation.getPilot().equals(memberStatusDto.getUri())) || 
                (reservation.getInstructor()!=null && reservation.getInstructor().equals(memberStatusDto.getUri()))
        ).toList();

        logger.trace("NOTIFY reservationsToUpdate after filter:" + reservations.size());

        List<Reservation> modifiedReservations = new ArrayList<Reservation>();




        for(Reservation res : reservations){
            logger.trace("NOTIFY resid=" + res.getId());
            boolean isPilot = res.getPilot()!=null && res.getPilot().equals(memberStatusDto.getUri());
            boolean isInstructor = res.getInstructor() != null && res.getInstructor().equals(memberStatusDto.getUri());

            boolean valid = true;

            valid &= memberStatusDto.getStatus().getStatus().equals("student") || memberStatusDto.getStatus().getStatus().equals("active");
            logger.trace("NOTIFY valid1=" + valid);
            valid &= !(isPilot && memberStatusDto.getStatus().getStatus().equals("student") && res.getInstructor().toString().isEmpty());
            logger.trace("NOTIFY valid2=" + valid);
            valid &= !(isInstructor && memberStatusDto.getStatus().getStatus().equals("student"));
            logger.trace("NOTIFY valid3=" + valid);

            boolean hasInstrLicense = false;
            for(String license: memberStatusDto.getStatus().getLicenses()){
                hasInstrLicense |= license.equals("instructor");
            }

            valid &= !isInstructor || (isInstructor & hasInstrLicense);
            logger.trace("NOTIFY valid4=" + valid);

            if(!valid){
                res.setValid(false);
                modifiedReservations.add(res);
            }
        }

        resRepo.saveAll(modifiedReservations);

        return ResponseEntity.ok().build();
    }


    Integer getPlaneId(URI planeUri){
        try{
            var parts = planeUri.getPath().split("/");
            return Integer.parseInt(parts[parts.length - 1]);
        }catch(Exception ex){
            throw new ResponseStatusException(
                    HttpStatus.NOT_ACCEPTABLE, "Malformed plane uri"
            );
        }
    }
}
