package cz.fel.storedav.swa.reservations.api;

import cz.fel.storedav.swa.members_client.api.MemberApiClient;
import cz.fel.storedav.swa.members_client.api.MembersApi;
import cz.fel.storedav.swa.members_client.api.MembersApiClient;
import cz.fel.storedav.swa.members_client.model.MemberDto;
import cz.fel.storedav.swa.members_client.model.MemberShortDto;
import cz.fel.storedav.swa.members_client.model.MemberStatusDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

@FeignClient(
        contextId = "Members",
        name = "${membersreg.name:members-register}",
        url = "${membersreg.url}",
        fallback = MembersServiceApiClient.MembersServiceApiClientFallback.class,
        configuration = MembersApiConfiguration.class
)
public interface MembersServiceApiClient extends MembersApi {
    Logger logger = LoggerFactory.getLogger(MembersServiceApiClient.class);

    @Component
    class MembersServiceApiClientFallback implements MembersServiceApiClient {

        @Override
        public ResponseEntity<List<MemberShortDto>> membersGet(String name, String license, String status) {
            logger.warn("membersGet fallback");
            return ResponseEntity.notFound().build();
        }

        @Override
        public ResponseEntity<URI> membersPost(MemberDto memberDto) {
            logger.warn("membersPost fallback");
            throw new UnsupportedOperationException("membersPost should not be called here...");
        }
    }
}
