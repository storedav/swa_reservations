package cz.fel.storedav.swa.reservations.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.net.URI;
import java.time.OffsetDateTime;

@Entity
@Table(name = "reservation")
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "plane")
    private URI plane;

    @Column(name="start_time")
    private OffsetDateTime startTime;

    @Column(name="end_time")
    private OffsetDateTime endTime;

    @Column(name="pilot")
    private URI pilot;

    @Column(name="instructor")
    private URI instructor;

    @Column(name="valid")
    private Boolean valid;

    @Column(name="type")
    @Pattern(regexp = "^personal|school|demo$")
    private String type;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public URI getPlane() {
        return plane;
    }

    public void setPlane(URI plane) {
        this.plane = plane;
    }

    public OffsetDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(OffsetDateTime startTime) {
        this.startTime = startTime;
    }

    public OffsetDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(OffsetDateTime endTime) {
        this.endTime = endTime;
    }

    public URI getPilot() {
        return pilot;
    }

    public void setPilot(URI pilot) {
        this.pilot = pilot;
    }

    public URI getInstructor() {
        return instructor;
    }

    public void setInstructor(URI instructor) {
        this.instructor = instructor;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
