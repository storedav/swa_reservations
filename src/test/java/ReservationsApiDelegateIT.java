import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.matching.StringValuePattern;
import cz.fel.storedav.swa.reservations.Application;
import cz.fel.storedav.swa.reservations.api.MembersServiceApiClient;
import cz.fel.storedav.swa.reservations.api.PlaneServiceApiClient;
import cz.fel.storedav.swa.reservations.api.PlanesServiceApiClient;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;
import javax.annotation.PostConstruct;
import org.springframework.cloud.openfeign.EnableFeignClients;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static io.restassured.RestAssured.get;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
                "planesreg.url=http://localhost:9093",
                "planessreg.name=planes-register",
                "membersreg.url=http://localhost:9093",
                "membersreg.name=members-register"
        }
)
@EnableConfigurationProperties
public class ReservationsApiDelegateIT {
    @LocalServerPort
    private int port;

    private String uri;

    final private static int mockPort = 9093;
    final private static String mockUrl = "http://localhost:" + mockPort;

    @Autowired
    private PlaneServiceApiClient reservationsServiceApiClient;

    @Autowired
    private MembersServiceApiClient membersServiceApiClient;

    @RegisterExtension
    static WireMockExtension ext = WireMockExtension.newInstance()
            .options(wireMockConfig().port(mockPort))
            .configureStaticDsl(true)
            .build();

    @PostConstruct
    public void init() {
        uri = "http://localhost:" + port;
    }

    @BeforeEach
    public void testSetup(){
        RestAssured.port = port;
    }

    @Test
    @Sql(scripts = "/test-data-empty.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_getReservations_noData() {
        get(uri+"/reservations").then().assertThat().statusCode(204);
    }

    @Test
    @Sql(scripts = "/test-data-empty.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_postReservation() {
        OffsetDateTime start = OffsetDateTime.now().withNano(0);
        OffsetDateTime end = start.plusDays(1);


        stubFor(WireMock.get(WireMock.urlEqualTo("/plane/3"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-type", "application/json")
                        .withBody("{\n" +
                                "  \"type\": \"Bristell 5\",\n" +
                                "  \"registration\": \"ok-war18\",\n" +
                                "  \"category\": \"ull\",\n" +
                                "  \"seatsNumber\": 2,\n" +
                                "  \"desc\": \"Something here\",\n" +
                                "  \"active\": true\n" +
                                "}")
                ));

        stubFor(WireMock.get(WireMock.urlEqualTo("/member/1/state"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-type", "application/json")
                        .withBody("{\n" +
                                "  \"status\": \"active\",\n" +
                                "  \"licenses\": [\n" +
                                "    \"ull\"\n" +
                                "  ]\n" +
                                "}")
                ));

        RestAssured.given()
                .header("Content-type", "application/json")
                .and()
                .body("{\n" +
                        "  \"plane\": \"" + mockUrl + "/plane/3\",\n" +
                        "  \"startTime\": \"" + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(start) + "\",\n" +
                        "  \"endTime\": \"" + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(end) + "\",\n" +
                        "  \"pilot\": \"" + mockUrl + "/member/1\",\n" +
                        "  \"valid\": true,\n" +
                        "  \"type\": \"personal\"\n" +
                        "}")
                .when()
                .post("/reservations")
                .then()
                .statusCode(201);

        RestAssured.given().when()
                .get("/reservations")
                .then()
                .statusCode(200)
                .body("[0].plane", equalTo(mockUrl + "/plane/3"))
                .body("[0].startTime", equalTo(DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(start)))
                .body("[0].endTime", equalTo(DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(end)))
                .body("[0].reservation", equalTo(uri + "/reservation/1"));

        verify(getRequestedFor(urlEqualTo("/member/1/state")));

        verify(getRequestedFor(urlEqualTo("/plane/3")));
    }

    @Test
    @Sql(scripts = "/test-data-empty.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_postReservation_invalid() {
        OffsetDateTime start = OffsetDateTime.now().withNano(0);
        OffsetDateTime end = start.plusDays(1);


        stubFor(WireMock.get(WireMock.urlEqualTo("/plane/3"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-type", "application/json")
                        .withBody("{\n" +
                                "  \"type\": \"Bristell 5\",\n" +
                                "  \"registration\": \"ok-war18\",\n" +
                                "  \"category\": \"ull\",\n" +
                                "  \"seatsNumber\": 2,\n" +
                                "  \"desc\": \"Something here\",\n" +
                                "  \"active\": false\n" +
                                "}")
                ));

        stubFor(WireMock.get(WireMock.urlEqualTo("/member/1/state"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-type", "application/json")
                        .withBody("{\n" +
                                "  \"status\": \"student\",\n" +
                                "  \"licenses\": [\n" +
                                "    \"ppl\"\n" +
                                "  ]\n" +
                                "}")
                ));

        RestAssured.given()
                .header("Content-type", "application/json")
                .and()
                .body("{\n" +
                        "  \"plane\": \"" + mockUrl + "/plane/3\",\n" +
                        "  \"startTime\": \"" + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(start) + "\",\n" +
                        "  \"endTime\": \"" + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(end) + "\",\n" +
                        "  \"pilot\": \"" + mockUrl + "/member/1\",\n" +
                        "  \"valid\": true,\n" +
                        "  \"type\": \"personal\"\n" +
                        "}")
                .when()
                .post("/reservations")
                .then()
                .statusCode(406);
    }

    @Test
    @Sql(scripts = "/test-data-empty.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_postReservation_conflict() {
        OffsetDateTime start = OffsetDateTime.now().withNano(0);
        OffsetDateTime end = start.plusDays(1);

        OffsetDateTime start2 = start.plusHours(12);
        OffsetDateTime end2 = start2.plusDays(1);



        stubFor(WireMock.get(WireMock.urlEqualTo("/plane/3"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-type", "application/json")
                        .withBody("{\n" +
                                "  \"type\": \"Bristell 5\",\n" +
                                "  \"registration\": \"ok-war18\",\n" +
                                "  \"category\": \"ull\",\n" +
                                "  \"seatsNumber\": 2,\n" +
                                "  \"desc\": \"Something here\",\n" +
                                "  \"active\": true\n" +
                                "}")
                ));

        stubFor(WireMock.get(WireMock.urlEqualTo("/member/1/state"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-type", "application/json")
                        .withBody("{\n" +
                                "  \"status\": \"active\",\n" +
                                "  \"licenses\": [\n" +
                                "    \"ull\"\n" +
                                "  ]\n" +
                                "}")
                ));

        RestAssured.given()
                .header("Content-type", "application/json")
                .and()
                .body("{\n" +
                        "  \"plane\": \"" + mockUrl + "/plane/3\",\n" +
                        "  \"startTime\": \"" + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(start) + "\",\n" +
                        "  \"endTime\": \"" + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(end) + "\",\n" +
                        "  \"pilot\": \"" + mockUrl + "/member/1\",\n" +
                        "  \"valid\": true,\n" +
                        "  \"type\": \"personal\"\n" +
                        "}")
                .when()
                .post("/reservations")
                .then()
                .statusCode(201);

        RestAssured.given()
                .header("Content-type", "application/json")
                .and()
                .body("{\n" +
                        "  \"plane\": \"" + mockUrl + "/plane/3\",\n" +
                        "  \"startTime\": \"" + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(start2) + "\",\n" +
                        "  \"endTime\": \"" + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(end2) + "\",\n" +
                        "  \"pilot\": \"" + mockUrl + "/member/1\",\n" +
                        "  \"valid\": true,\n" +
                        "  \"type\": \"personal\"\n" +
                        "}")
                .when()
                .post("/reservations")
                .then()
                .statusCode(409);
    }


    @Test
    @Sql(scripts = "/test-data-empty.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_notifyMemberChange() {
        OffsetDateTime start = OffsetDateTime.now().withNano(0);
        OffsetDateTime end = start.plusDays(1);

        stubFor(WireMock.get(WireMock.urlEqualTo("/plane/3"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-type", "application/json")
                        .withBody("{\n" +
                                "  \"type\": \"Bristell 5\",\n" +
                                "  \"registration\": \"ok-war18\",\n" +
                                "  \"category\": \"ull\",\n" +
                                "  \"seatsNumber\": 2,\n" +
                                "  \"desc\": \"Something here\",\n" +
                                "  \"active\": true\n" +
                                "}")
                ));

        stubFor(WireMock.get(WireMock.urlEqualTo("/member/1/state"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-type", "application/json")
                        .withBody("{\n" +
                                "  \"status\": \"active\",\n" +
                                "  \"licenses\": [\n" +
                                "    \"ull\"\n" +
                                "  ]\n" +
                                "}")
                ));

        RestAssured.given()
                .header("Content-type", "application/json")
                .and()
                .body("{\n" +
                        "  \"plane\": \"" + mockUrl + "/plane/3\",\n" +
                        "  \"startTime\": \"" + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(start) + "\",\n" +
                        "  \"endTime\": \"" + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(end) + "\",\n" +
                        "  \"pilot\": \"" + mockUrl + "/member/1\",\n" +
                        "  \"valid\": true,\n" +
                        "  \"type\": \"personal\"\n" +
                        "}")
                .when()
                .post("/reservations")
                .then()
                .statusCode(201);


        RestAssured.given()
                .header("Content-type", "application/json")
                .and()
                .body("{\n" +
                        "  \"status\": {\n" +
                        "    \"status\": \"grounded\",\n" +
                        "    \"licenses\": [\n" +
                        "      \"ull\"\n" +
                        "    ]\n" +
                        "  },\n" +
                        "  \"uri\": \"" + mockUrl + "/member/1\"\n" +
                        "}")
                .when()
                .post("/notify/change/member")
                .then()
                .statusCode(200);

        RestAssured.given()
                .when()
                .get("/reservation/1")
                .then()
                .statusCode(200)
                .body("$.valid", equalTo("false"));
    }

}
